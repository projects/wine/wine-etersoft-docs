# Copyright (c) 2005-2009 Etersoft
# Permission is granted to copy, distribute and/or modify this document
# under the terms of the GNU Free Documentation License, Version 1.3
# or any later version published by the Free Software Foundation;

>> Использование WINE\@Etersoft
>>> Запуск win-приложений
# Когда приложение уже установлено, или если оно вообще не требует установки, можно запускать
# на любой из дисков wine -- должна находиться в области видимости -- в том каталоге, который виден в wine
Общее правило для запуска всех win-приложений в WINE -- запускаемые файлы должны находиться в [[wineenv]] <области видимости|область видимости> WINE, то есть на одном из логических дисков WINE или в его подкаталогах. 
Если программа поставляется на компакт-диске, то не забудьте должным образом смонтировать диск[[Нужно делать это вручную, или монтирование выполняется автоматически -- зависит от вашего дистрибутива и стиля работы.]], прежде чем обращаться к нему из WINE. Обратите внимание, что в этом случае у вас должен быть разрешён запуск приложений с компакт-диска. Если приложение распространяется не на диске -- не забудьте сначала скопировать его в область видимости WINE.

Запуск win-приложений производится двойным щелчком мыши на значке в любом файловом менеджере.
@@ Запуск программы  @ ../common/images/install-desktop.png @ install-desktop @@

Также приложение может быть запущено с помощью команды в командной строке.
Не забудьте сначала перейти в каталог с программой.

Для запуска exe-файлов нужно выполнить команду:

^$ wine программа.exe^

@@ Запуск программы в консоли @ ../common/images/install-console.png @ install-console @@

Если программа предназначена исключительно для работы в среде Windows 95/98, запускайте её командой:

^$ wine98 программа.exe^

Программы, поставляемые в виде msi-пакетов, а также файлы ^.bat^ и ^.cmd^ запускаются с помощью команды:

^$ wine start пакет.msi^

Для запуска консольных приложений, например файлового менеджера ^Far^, используется команда:

^$ wineconsole Far.exe^

Для получения командной строки запустите

^$ wineconsole cmd^

или выберите в меню программ пункт "Командная строка WINE".


Запуск программ DOS (16-разрядных программ реального режима процессора), особенно сложных,
в штатной виртуальной DOS-машине, имеющейся в WINE, обычно не даёт положительного результата.
Рекомендуется использовать ^dosemu^ или ^dosbox^.

# Убрал пока, чтобы не затрудняться описанием:
#Для того, чтобы можно было запускать win-приложения как обыкновенные исполняемые файлы host-системы
#(например, из консольного файлового менеджера Midnight Commander),
#должна быть запущена системная служба wine.
#Обычно система настроена на автоматический запуск этого сервиса.


# иду в каталог в host-системе иду туда и запускаю 
# wine <имяпрограммы.exe> -- запустится в обычном иксовом окне
# GUI
# в winefile пойти и запустить оттуда просто кликнув
# способы запуска win-приложений (из меню, и нутри wine etc)
## отдельно описать wine-утилиты: autorun, filebrowser, notepad...
## частные вопросы использования: 

При запуске программы в WINE на самом деле запускается не только сама программа,
но и несколько вспомогательных, в частности, программа ^wineserver^, реализующая
функции ядра Windows, и предназначенная для синхронизации различных win-программ,
запущенных пользователем.

>>> Запуск сервисов

Некоторые win-приложения должны быть запущены как сервисы. От обычных программ сервисы отличаются тем,
что не ведут диалога с пользователем, и могут выполняться незаметно.

При запуске сервисов следует иметь в виду, что они завершаются вместе с завершением ^wineserver^,
поэтому следует предварительно запустить ^wineserver^ с ключом ^-p^, отменяющим автоматическое завершение.

@@ Пример запуска сервиса @ @@
$ wineserver -p
$ wine pssvc.exe &
@@

В указанном примере программа ^pssvc^ будет запущена как сервис, причём в фоновом режиме.

[Обратите внимание, что из-под программы ^mc^ запускать программы в фоновом режиме нельзя.]

>>> Установка и удаление win-приложений

Как и в Windows, перед использованием большую часть win-приложений сначала потребуется установить. Установка производится обычным для Windows способом -- с помощью поставляемой вместе с win-приложением программы установки. Разница в том, что в случае WINE программа будет установлена в локальном win-окружении пользователя. 

Для установки win-приложения следует любым удобным способом запустить программу установки (чаще всего ^setup.exe^). Дальше можно действовать по инструкции, предлагаемой поставщиком win-приложения.

# если приложение просит перезагрузиться после установки, то нужно запустить wineboot 
# при такой перезагрузке выходить ни откуда не нужно
# wineboot выполняет действия, назначенные на момент загрузки Windows - устанавливает DLL, регистрирует сервисы, запускает программы, назначенные для автозапуска
Многие win-приложения запрашивают перезагрузку для завершения установки.
Перезагружать host-систему при этом не следует. В локальном win-окружении процедуре загрузки Windows соответствует команда ^wineboot^ -- её можно вызвать из любой командной строки. Если в этот момент в WINE выполняются другие приложения, то рекомендуется их завершать до перезагрузки.

# штатную программу удаления или просто удалить каталог с программой
Для удаления win-приложения, установленного в win-окружении, следует воспользоваться программой ^uninstaller^.
Запустить её можно через меню или командой ^wine uninstaller^. Эта утилита выводит список
установленных в win-окружении приложений (если они зарегистрированы в реестре). Чтобы удалить приложение, выберите его из списка и нажмите кнопку ``Uninstall''. Если в списке нет приложения, которое вы хотите удалить, то достаточно просто удалить каталог с приложением (можно воспользоваться для этого программой ^winefile^, а можно -- стандартными средствами host-системы). 

Иногда приложение требует дополнительные компоненты, отсутствующие в стандартной поставке WINE.
В этом случае можно обратиться к программе ^winetricks^, запустив её из командной строки.
Она позволяет установить различные компоненты, при этом все необходимые вспомогательные
действия берёт на себя. Используйте с осторожностью, установка некоторых компонент или
их сочетание может сломать работающее win-окружение.

>>> Создание ярлыков

Создание ярлыков для программы выполняется штатными средствами и особых отличий от
создания ярлыка для Линукс-программы не имеет.

Рассмотрим создание ярлыка на примере программы <Блокнот WINE> (^C:\\windows\\system32\\notepad.exe^).
Для запуска программы через WINE потребуется указать полный путь к ней,
поэтому при создании ярлыка нужно указать команду запуска

^wine "C:\\windows\\system32\\notepad.exe"^

Обратите внимание, что для многих программ важен текущий каталог, поэтому его следует отдельно
указать в ярлыке, причём это должен быть путь в формате host-системы, то есть

^/home/user/wine_c/windows/system32^.

Как правило, ярлык для программ создаётся ими самими при установке и размещается на <Рабочем столе>.

>>> Создание резервной копии

Перед установкой новой программы, или перед существенными изменениями
желательно сделать копию (бэкап) настроек WINE и установленных программ.

Для того, чтобы создать полную копию, достаточно заархивировать каталог ^~/.wine^.
Полученный архив можно использовать для восстановления после сбоя, либо
для тиражирования установленной программы -- передать архив другому пользователю,
чтобы ему не пришлось выполнять те же настройки.

Если программ установлено много, будет достаточно сохранить только копии
файлов реестра. Файлы реестра хранятся в каталоге ^~/.wine^ и имеют расширение ^.reg^.

>>> Восстановление WINE

Если были повреждены системные файлы или реестр, вполне возможно,
что переустановка не требуется, достаточно выполнить восстановление.
Выберите в меню программ пункт "Восстановление WINE". Того же можно
достичь выполнением команды

^wine --update^

>>> Диагностика проблем

Для облегчения диагностики проблем с установкой и настройкой WINE\@Etersoft в него
входит программа ^winediag^, выводящая перечень проверенных параметров и выводящая
сообщение о конкретной проблеме.
В конце вывода указывается код ошибки. Возможны следующие коды:


. WE13 -- не установлен пакет wine-etersoft, возможно в системе имеется Wine другой сборки
. WE14, WE15 -- не удаётся открыть библиотеку libwine, пакеты WINE\@Etersoft установлены некорректно;
. WE16 -- не удаётся открыть библиотеку закрытой части WINE\@Etersoft;
. WE17 -- библиотека закрытой части WINE\@Etersoft повреждена;
. WE18 -- ошибки в получении информации о лицензии; возможно, повреждён файл лицензии или библиотека закрытой части;
. WE20 -- загружена устаревшая версия модуля etercifs
. WE52 -- не найдена библиотека freetype, через которую производится отрисовка символов;
. WE54 -- не работает функция dlopen или не доступна библиотека freetype;
. WE52 -- не удаётся инициализировать библиотеку freetype;
. WE53 -- библиотека freetype слишком старой версии;
. WE55 -- нет соединения с Икс-сервером; возможно, не установлена переменная DISPLAY или нет прав. Нужно зайти в графику нормальным образом;
. WE56 -- неверная модель потоков, работать ничего не будет, либо ошибка в самой программе диагностики;
. WE57 -- не удаётся узнать лимиты на количество файлов, либо они слишком малы;
. WE58 -- не удаётся узнать лимит виртуальной памяти;
. WE61 -- не удалось установить блокировку большого размера на файл (возможно, нет поддержки больших файлов);
. WE62 -- не удалось установить блокировку; возможно, проблема в файловой системе или в самом тесте;
. WE63 -- слишком ограничена маска файлов (umask); совместная работа с группой над файлами невозможна;
. WE64 -- неверно установлена маска файлов (umask); нет доступа даже к собственным файлам;
. WE65 -- локаль не установлена (возможно, в системе отсутствует поддержка локалей или ^winediag^ запущен под пользователем ^root^);
. WE66 -- установлена английская локаль, что может вызвать проблемы в работе программ, рассчитанных на русскую локаль;
. WE67 -- установлена неверная версия Windows, либо версию не удалось получить;
. WE68 -- установленная закрытая часть WINE\@Etersoft не совместима с открытой (разные версии).
