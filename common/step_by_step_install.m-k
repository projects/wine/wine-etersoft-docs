# Copyright (c) 2005-2009 Etersoft
# Permission is granted to copy, distribute and/or modify this document
# under the terms of the GNU Free Documentation License, Version 1.3
# or any later version published by the Free Software Foundation;

>> Пошаговая инструкция по установке rpm-пакетов WINE\@Etersoft

Далее приведена пошаговая инструкция по установке WINE\@Etersoft Network на примере системы с rpm-пакетами.
Переходите к выполнению следующего шага только после проверки, что предыдущий шаг выполнен успешно.

[ВНИМАНИЕ! Выполнение отдельных команд требует полномочий суперпользователя (root).
Знак "#" в приглашении означает, что команду нужно выполнить от имени пользователя root.
Знак "$" в приглашении означает, что команду следует выполнять от имени обычного пользователя.
Запускайте команды из-под root только тогда, когда это действительно необходимо.]


>>> 1. Пакеты, которые необходимо установить

. wine-etersoft
. wine-etersoft-network

[ Для корректной работы приложений, использующих OpenGL, потребуется поставить пакет ^wine-etersoft-gl^. Официально функционирование GL поддерживается только в продукте WINE\@Etersoft CAD. ]

>>> 2. Если установлен стандартный WINE, его следует удалить

Перед установкой пакетов необходимо убедиться в отсутствии уже установленных пакетов WINE:

    ^# rpm -qa | grep wine^


если команда вывела на экран хоть один пакет, скорее всего, он относится к WINE и его необходимо удалить.
Как правило, это будут пакеты wine и libwine. Такие пакеты, как libkwineffects и docs-wine-intro, удалять
не нужно.
Введите следующую команду (требуются привилегии root) для удаления пакетов:

    ^# rpm -e пакеты_wine,_установленные_в_системе^


ПРОВЕРКА: если команда выполнилась корректно, то пакетов, относящихся к wine, не должно быть в выводе команде:

    ^# rpm -qa | grep wine^


>>> 3. Удалить каталог ^.wine^ из домашнего каталога пользователя

При использовании WINE\@Etersoft с настройками от обычного WINE могут возникнуть проблемы, поэтому мы рекомендуем переименовать,
либо удалить каталог .wine:

    ^$ rm -rf ~/.wine^


ПРОВЕРКА: если каталог удалён, вывод команды ^ls ~/.wine^ должен быть примерно таким:

^ls: .wine: No such file or directory^


[Примечание: Каталог ^.wine^ следует удалить из домашних каталогов всех пользователей, которые будут запускать WINE.]

>>> 4. Установить пакеты WINE\@Etersoft

Устанавливаем пакеты WINE\@Etersoft. Для этого, находясь в каталоге с пакетами, вводим команду (требуются привилегии root):
@@Установка пакетов@ @@
test:~ # ls -1 | grep wine
wine-etersoft-1.0.12-eter2mdv.i586.rpm
wine-etersoft-network-1.0.12-eter2mdv.i586.rpm

test:~ #  rpm -Uvh wine-etersoft*.rpm
Подготовка...     ########################################### [100%]
   1:wine-etersoft ########################################## [ 33%]
   2:wine-etersoft-network ################################## [ 67%]
WINE: Registering binary handler for Windows program         [ DONE ]
Running etersafed...                                         [ DONE ]
@@


ПРОВЕРКА: 
@@Список установленных пакетов@ @@
test:~ # rpm -qa | grep wine
wine-etersoft-1.0.12-eter2mdv
wine-etersoft-network-1.0.12-eter2mdv
@@


>>> 5. Первый запуск WINE

При первом запуске WINE необходимо создать win-окружение для каждого пользователя.
Для этого нужно выполнить команду


    ^$ wine^

и дождаться её завершения. Процесс создания окружения отображается на графической заставке.

ВЫВОД (может различаться при разных системах и конфигурациях!):

@@Создание win-окружения@ @@
First running... Using WINEPREFIX=/net/wine/.wine
Creating default file tree...
Copying prepared tree from '/usr/share/wine/skel' ...
Initialize registry and environments...
Building local environment...
Flash Player 9 NPAPI installing...
Device 'e:' created as link for '/media' target.
Device 'e::' created as link for '/dev/cdrom' target.
Device 'd:' created as link for '/net/wine/Documents' target.
Communication dlls installing...
MSI installing...
Windows Scripting installing...
Successfully registered DLL msxml3.dll
Successfully registered DLL msxml4.dll
Successfully registered DLL mfc40.dll
Successfully registered DLL mfc42.dll
Successfully registered DLL msscript.ocx
Successfully registered DLL mfc42u.dll
WINE@Etersoft 1.0 SQL 1.0.10-eter16/11

Licensed for ООО "Этерсофт" with registration number ****-****
Contact person: Системный администратор
Use /net/wine/wine_c as WINE C:\ disk.
Copy your program into and install it.
@@ 

ПРОВЕРКА:
@@Содержимое каталога .wine@ @@
$ ls -la ~/.wine

drwxrwxr-x  3 test test   4096 Окт 30 11:31 .
drwx------ 28 test test   4096 Окт 30 11:45 ..
drwxrwxr-x  4 test test   4096 Окт 30 11:31 dosdevices
-rw-rw-r--  1 test test    177 Окт 30 11:31 .etersoft-release
-rw-rw-r--  1 test test     72 Окт 30 11:31 install.log
-rw-rw-r--  1 test test 573550 Окт 30 11:31 system.reg
-rw-rw-r--  1 test test   3801 Окт 30 11:31 userdef.reg
-rw-rw-r--  1 test test  26467 Окт 30 11:31 user.reg
@@

Не забудьте скопировать файл лицензии ^WINE-ETERSOFT.LIC^ (ссылка 
на него присылается в письме при заказе продукта) в один
из каталогов: ^~/.wine^, ^C:\\WINDOWS\\INF^ или в ^/etc/wine^.
Если файл лицензии не будет найден, это будет сообщено при выводе
сведений и версии wine.

ПРОВЕРКА:
@@Вывод сведений о версии wine@ @@
$ wine --version
WINE@Etersoft 1.0 SQL 1.0.12-eter2/3

Licensed for ООО "Этерсофт" with registration number ****-****
Contact person: Системный администратор
License expired at 28/04/2010
@@

<Вы установили WINE\@Etersoft!>

>>> 6. Установка приложений в WINE

Можете приступать к установке win-приложений. В роли диска ^С:^ выступает каталог ^~/wine_c^ (находится
в домашнем каталоге).

Скопируйте туда дистрибутив программы и выполните команду

    ^$ wine имя_программы.exe^


########################################################################
>> Пошаговая инструкция по установке deb-пакетов WINE\@Etersoft

Далее приведена пошаговая инструкция по установке WINE\@Etersoft SQL на примере системы с deb-пакетами.
Переходите к выполнению следующего шага только после проверки, что предыдущий шаг выполнен успешно.

[ВНИМАНИЕ! Выполнение отдельных команд требует полномочий суперпользователя (root).
Знак "#" в приглашении означает, что команду нужно выполнить от имени пользователя root.
Знак "$" в приглашении означает, что команду следует выполнять от имени обычного пользователя.
Запускайте команды из-под root только тогда, когда это действительно необходимо.]


>>> 1. Пакеты, которые необходимо установить

. wine-etersoft
. wine-etersoft-sql

[ Для корректной работы приложений, использующих OpenGL, потребуется поставить пакет ^wine-etersoft-gl^. Официально функционирование GL поддерживается только в продукте WINE\@Etersoft CAD. ]

>>> 2. Если установлен стандартный WINE, его следует удалить

Перед установкой пакетов необходимо убедиться в отсутствии уже установленных пакетов WINE:

    ^$ dpkg -l | grep wine_^


если команда вывела на экран хоть один пакет, скорее всего, он относится к WINE и его необходимо удалить.
Как правило, это будут пакеты wine и libwine. Такие пакеты, как libkwineffects и docs-wine-intro, удалять
не нужно.

    ^# dpkg -P пакеты_wine,_установленные_в_системе^
C параметром -P пакет будет удалён вместе с конфигурационными файлами.

ПРОВЕРКА: если команда выполнилась корректно, то вывод следующей команды на экран должен быть пустым:

    ^$ dpkg -l | grep wine_^


>>> 3. Удалить каталог ^.wine^ из домашнего каталога пользователя

При использовании WINE\@Etersoft с настройками от обычного WINE могут возникнуть проблемы, поэтому мы рекомендуем переименовать,
либо удалить каталог .wine:

    ^$ rm -rf ~/.wine^


ПРОВЕРКА: если каталог удалён, вывод команды ^ls ~/.wine^ должен быть примерно таким:

^ls: .wine: No such file or directory^


[Примечание: Каталог ^.wine^ следует удалить из домашних каталогов всех пользователей, которые будут запускать WINE.]

>>> 4. Установить пакеты WINE\@Etersoft

Устанавливаем пакеты WINE\@Etersoft. Для этого, находясь в каталоге с пакетами, вводим команду (требуются привилегии root):
@@Установка пакетов@ @@
# ls -1 | grep wine
wine-etersoft_1.0.11-eter8ubuntu_i386.deb
wine-etersoft-sql_1.0.11-eter3ubuntu_i386.deb

#  dpkg -i wine-etersoft*.deb
Подготовка...     ########################################### [100%]
   1:wine-etersoft ########################################## [ 33%]
   2:wine-etersoft-sql ###################################### [ 67%]
WINE: Registering binary handler for Windows program         [ DONE ]
Running etersafed...                                         [ DONE ]
@@


ПРОВЕРКА:
@@Список установленных пакетов@ @@
test:~ # dpkg -l | grep wine
ii  wine-etersoft                              1.0.11-eter8ubuntu
ii  wine-etersoft-sql                          1.0.11-eter3ubuntu
@@


>>> 5. Первый запуск WINE

При первом запуске WINE необходимо создать win-окружение для каждого пользователя.
Для этого нужно выполнить команду


    ^$ wine^

и дождаться её завершения. Процесс создания окружения отображается на графической заставке.

ВЫВОД (может различаться при разных системах и конфигурациях!):

@@Создание win-окружения@ @@
First running... Using WINEPREFIX=/home/baraka/.wine with WINE@Etersoft 1.0 SQL 1.0.11-eter8/3
Creating default file tree...
Copying prepared tree from '/usr/share/wine/skel' ...
Initialize registry and environments...
Building local environment...
Flash Player 9 NPAPI installing...
Device 'd:' created as link for '/home/baraka/' target.
Device 'u:' created as link for 'unc/server/share' target.
MDAC 2.7 installing...
MSJET 4.0 installing...
Communication dlls installing...
MSI installing...
Windows Scripting installing...
Successfully registered DLL msxml3.dll
Successfully registered DLL msxml4.dll
Successfully registered DLL mfc40.dll
Successfully registered DLL mfc42.dll
Successfully registered DLL msscript.ocx
Successfully registered DLL mfc42u.dll

WINE@Etersoft 1.0 SQL 1.0.11-eter8/3
Product: WINE@Etersoft 1.0 SQL
Licensed for ООО "Этерсофт" with registration number ****-****
Contact person: Системный администратор
WINE@Etersoft has been configured for the first time.
Use /home/baraka/wine_c as WINE C:\ disk.
Copy your program into and install it.
@@ 

ПРОВЕРКА:
@@Содержимое каталога .wine@ @@
$ ls -la ~/.wine

drwxrwxr-x   3 baraka users    115 2009-09-10 18:05 .
drwx------ 102 baraka users   8192 2009-09-10 18:00 ..
drwxrwxr-x   4 baraka users     53 2009-09-10 18:02 dosdevices
-rw-rw-r--   1 baraka users    284 2009-09-10 18:05 .etersoft-release
-rw-rw-r--   1 baraka users      0 2009-09-10 18:05 install.log
-rw-rw-r--   1 baraka users 825294 2009-09-10 18:05 system.reg
-rw-rw-r--   1 baraka users   3554 2009-09-10 18:01 userdef.reg
-rw-rw-r--   1 baraka users  31191 2009-09-10 18:05 user.reg
@@

Не забудьте скопировать файл лицензии ^WINE-ETERSOFT.LIC^ (ссылка 
на него присылается в письме при заказе продукта) в один
из каталогов: ^~/.wine^, ^C:\\WINDOWS\\INF^ или в /etc/wine.
Если файл лицензии не будет найден, это будет сообщено при выводе
сведений и версии wine.

ПРОВЕРКА:
@@Вывод сведений о версии wine@ @@
$ wine --version
WINE@Etersoft 1.0 SQL 1.0.11-eter8/3

Product: WINE@Etersoft 1.0 SQL
Licensed for ООО "Этерсофт" with registration number ****-****
Contact person: Системный администратор
License expired at 13/11/2009
@@

<Вы установили WINE\@Etersoft!>

>>> 6. Установка приложений в WINE

Можете приступать к установке win-приложений. В роли диска ^С:^ выступает каталог ^~/wine_c^ (находится
в домашнем каталоге).

Скопируйте туда дистрибутив программы и выполните команду

    ^$ wine имя_программы.exe^
