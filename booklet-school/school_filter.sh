#!/bin/sh
# \\ and \@
cat |
	sed -e "s|splash\.png|school-splash\.png|g" | \
	sed -e "s|install-console\.png|school-install-console\.png|g" | \
	sed -e "s|ние WINE\\\@Etersoft|ние Школьного Вайна|g" |
	sed -e "s|ка WINE\\\@Etersoft|ка Школьного Вайна|g" |
	sed -e "s|ков WINE\\\@Etersoft|ков Школьного Вайна|g" |
	sed -e "s| с WINE\\\@Etersoft| со Школьным Вайном|g" |
	sed -e "s|WINE\\\@Etersoft|Школьный Вайн|g" | \
	sed -e "s|Unix-|Линукс-|g" | \
#	sed -e "s|ка WINE\@Etersoft|ка Школьного Вайна|g"
	cat

