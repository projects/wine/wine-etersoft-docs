#!/bin/sh
# 2006, 2007 (c) Etersoft www.etersoft.ru
# Public domain

. /usr/share/eterbuild/eterbuild

# FIXME: attach korinf functions!
# get distro list from arg/directory/file
# from DIR/distro.list if arg is dir
# from FILE if arg is file name
# from ARG if arg is list
get_distro_list()
{
	local i
	local LIST
	local DIR

	# get list from directory
	if [ -d "$1" ] ; then
		LIST="$1/distro.list"
		if [ ! -r "$LIST" ] ; then
			fatal "get_distro_list: $LIST is not found"
		fi
		DIR=$1
	# get list from file
	elif [ -r "$1" ] ; then
		LIST="$1"
		DIR=`dirname $1`
	# list in the variable
	else
		echo "$@"
		return
	fi

	cat "$LIST" | grep -v "^#" | grep -v "^\." | sed -e "s|[ 	].*||g" | grep -v "^\$"
	# get list included files
	LIST=`cat "$LIST" | grep "^\." | sed -e "s|^\. ||g"`
	for i in $LIST ; do
		test -f "$DIR/$i" || continue
		get_distro_list $DIR/$i
	done
}

# sorted in correct numeric order
get_sorted_distro_list()
{
	get_distro_list "$1" | sort -t / -k1,1 -k2,2n
}


export WINENUMVERSION="testing"

WINEPUB_PATH=/var/ftp/pub/Etersoft/WINE@Etersoft
PATHTO=$WINEPUB_PATH/$WINENUMVERSION/WINE
# local list, created from real dirs
#FORSALES=distro.list
#WINESITE=http://etersoft.ru/download/WINE%40Etersoft-1.0/WINE

LISTFILE=platforms.m-k
DATE=`date "+%F"`

cat <<EOF >$LISTFILE
# Generated with $0. Do not edit!
> WINE\@Etersoft: поддерживаемые операционные системы

Обратите внимание, что поддерживаются только системы GNU/Linux с ядрами 2.6.x.

Системы FreeBSD, Solaris, Mac OS X и Windows 7 находятся в планах поддержки.
EOF
#>$FORSALES
DISTR_LIST=$(get_sorted_distro_list $PATHTO/../distro.list)
#echo $DISTR_LIST
for i in $DISTR_LIST; do
	ASTER=
	LI=
	test -L $PATHTO/$i && LI=" (собрано в `readlink $PATHTO/$i | sed -e "s|\.\./||g;s|/| |g"`)"
	FILENAME=$(ls -1 $PATHTO/$i/wine-etersoft* | head -n1)
	echo $FILENAME
	test -f "$FILENAME" || LI=" (на $DATE ещё не готово)"
	echo $i | grep Special >/dev/null && continue
	echo $i | grep FreeBSD >/dev/null && ASTER=" *"
	echo $i | grep Solaris >/dev/null && ASTER=" **"
	echo $i | grep Mac >/dev/null && ASTER=" **"
	#echo ". @${i/\// }$LI|$WINESITE/$i@$ASTER" >>$LISTFILE
	DISTR=$(echo "$i" | sed -e "s|@|\\\\@|g")
	SYS=$(echo "${DISTR/\/*/}")
	if [ "$SYS" = "x86_64" ] ; then
		DISTR=$(echo "$DISTR" | sed -e "s|x86_64/||g")
		SYS="$(echo "${DISTR/\/*/}") x86_64"
		echo $DISTR $SYS
	fi

	echo $SYS
	if [ "$SYS" != "$oldSYS" ] ; then
		echo "" >>$LISTFILE
		echo ". <$SYS>" >>$LISTFILE
	fi
	echo ".. ${DISTR/\// }$LI$ASTER" >>$LISTFILE
	#LI=`echo $LI | iconv -f koi8-r -t utf8`
	#test -z "$ASTER" && 
	#echo -e "$i\t${i/\// } $LI" >>$FORSALES
	oldSYS=$SYS

done
cat >>$LISTFILE <<EOF

* операционная система имеет ограниченную поддержку. Возможны проблемы при использовании WINE\@Etersoft.
** готовится поддержка
EOF
