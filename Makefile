# General make
#
# Copyright (c) 2005-2009 Etersoft
# Permission is granted to copy, distribute and/or modify this document
# under the terms of the GNU Free Documentation License, Version 1.3
# or any later version published by the Free Software Foundation;

topdir=.
include ./Makefile.rules

SUBDIRS=tex common/images booklet booklet-school manual readme license

all:
	for i in $(SUBDIRS) ; do \
		$(MAKE) -C $$i ; \
	done
	
clean: 
	for i in $(SUBDIRS) ; do \
		$(MAKE) -C $$i clean ; \
	done

