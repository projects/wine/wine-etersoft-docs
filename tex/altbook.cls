\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{altbook}[2005/09/29 ALT Linux publications design]
% These are necessary to define macro nicely
\RequirePackage{ifthen,calc}
\RequirePackage{graphicx}
\RequirePackage[a5paper]{geometry}
\RequirePackage{fancyhdr}
% Booleans used in options
\newboolean{usemtc}
\newboolean{inmaintoc}
\newboolean{withpics}
\newboolean{bottomed}
\newboolean{marginpars}
\newlength{\alt@textwidth}
\newlength{\alt@textheight}
\newlength{\alt@paperwidth}
\newlength{\alt@paperheight}
% Let's define our options
\DeclareOption{dvdbox}{%
  \setlength{\alt@paperwidth}{125mm}
  \setlength{\alt@paperheight}{183mm}
  \PassOptionsToClass{a5paper}{extbook}
}
\DeclareOption{60x90}{%
  \setlength{\alt@paperwidth}{138mm}
  \setlength{\alt@paperheight}{210mm}
  \PassOptionsToClass{a5paper}{extbook}
}
\DeclareOption{a5}{%
  \setlength{\alt@paperwidth}{148mm}
  \setlength{\alt@paperheight}{210mm}
  \PassOptionsToClass{a5paper}{extbook}
}
\DeclareOption{marginpars}{%
  \setboolean{marginpars}{true}
}
\DeclareOption{withpics}{%
  \setboolean{withpics}{true}
}
\DeclareOption{minitoc}{%
  \AtEndOfClass{\RequirePackage{minitoc}
  \setboolean{usemtc}{true}
  \renewcommand{\mtcfont}{\small\rmfamily\upshape}
  \renewcommand{\mtcSfont}{\sffamily\large\upshape}
  \renewcommand{\mtctitle}{}
  \setcounter{minitocdepth}{1}
  \setlength{\mtcindent}{0pt}}
}
\DeclareOption{pscyr}{%
  \AtEndOfClass{\RequirePackage{pscyr}
  \renewcommand{\rmdefault}{faq}
  \renewcommand{\ttdefault}{cmtt}
  \renewcommand{\sfdefault}{ftx}}
}
\DeclareOption{bottomed}{%
  \setboolean{bottomed}{true}
%  \AtEndOfClass{\geometry{foot=7mm}}
}
\DeclareOption{easy}{%
%  \raggedbottom
  \sloppy
  \hbadness = 5000        % don't print trivial gripes
}
\DeclareOption{footmisc}{%
  \AtEndOfClass{\RequirePackage[perpage,bottom,stable,multiple]{footmisc}}
}
\DeclareOption{footnpag}{%
  \AtEndOfClass{\RequirePackage{footnpag}}
}
\DeclareOption{debug}{%
  \AtEndOfClass{\geometry{showframe}}
}
\DeclareOption{binding}{%
  \AtEndOfClass{\geometry{bindingoffset=5mm}}
}
\DeclareOption{crop}{%
  \AtEndOfClass{\RequirePackage[a4,cross,dvips,mount1=1,center,noinfo]{crop}}
}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{extbook}}
%\ExecuteOptions{dvdbox}
\ProcessOptions\relax
\LoadClass{extbook}
% first of all some basic page setup
\geometry{papersize={\alt@paperwidth,\alt@paperheight},hscale=0.8,vscale=0.85,headsep=5mm,heightrounded,includehead,dvips}
\ifthenelse{\boolean{marginpars}}{%
  \geometry{marginparwidth=38pt}
  \geometry{includemp}
  \addtolength{\headwidth}{\marginparsep}
  \addtolength{\headwidth}{\marginparwidth}
}{%
}
\ifthenelse{\boolean{bottomed}}{%
  \geometry{includefoot}
}{%
  \geometry{nofoot}
}
%\setlength{\textheight}{170mm}
% Our basic printing preferences that do not change
\widowpenalty=10000
\clubpenalty=10000
\RequirePackage{indentfirst}
%%
%% Document matters
%%
\renewcommand\frontmatter{%
%    \cleardoublepage
  \setboolean{@mainmatter}{false}
  \pagenumbering{arabic}}
\renewcommand\mainmatter{%
  \setboolean{inmaintoc}{false}
  \ifthenelse{\isodd{thepage}}{%
    \clearpage\ %
    \thispagestyle{empty}}{}
  \clearpage
  \setboolean{@mainmatter}{true}
  % \pagenumbering{arabic}
}
%%
%% Headers and Footers
%%
% We'll use fancy mechanics

\pagestyle{fancy}
% headers look
\newcommand{\Folio}[1]{%
  \begingroup\sffamily\bfseries#1\endgroup
}
\newcommand{\headerfont}{\sffamily}
\newcommand{\Pagenum}{\Folio{\thepage}}
%\setlength{\headrulewidth}{.5pt}% old syntax of fancyheadings
\renewcommand{\headrulewidth}{.5pt}
\ifthenelse{\boolean{bottomed}}{% bottomed
%  \setlength\footwidth{\textwidth+1em}
  \lhead[\fancyplain{}{\textsf{\leftmark}}]{%
    \fancyplain{}{}
  }
  \rhead[\fancyplain{}{}]{%
    \fancyplain{}{\textsf{\rightmark}}
  }
  \lfoot[\fancyplain{}{\Pagenum}]{}
  \rfoot[]{\fancyplain{}{\Pagenum}}
  \cfoot{}
}{% topped
  \ifthenelse{\boolean{marginpars}}{}{\setlength\headwidth{\textwidth+1em}}
  \lhead[\fancyplain{}{\Pagenum}]{%
    \fancyplain{}{\textsf{\rightmark}}
  }
  \rhead[\fancyplain{}{\textsf{\leftmark}}]{%
    \fancyplain{}{\Pagenum}%
  }
  \chead{}
  \lfoot{}
  \rfoot{}
}
% provide that no page numbers will appear on a plain page
\cfoot[\fancyplain{}{}]{%
  \fancyplain{}{}%
}
% What marks should look like and where they should appear
    \def\chaptermark#1{%
      \markboth {%
        \ifnum \c@secnumdepth >\m@ne
          \if@mainmatter
            {\headerfont\@chapapp\space \thechapter. \space} %
          \fi
        \fi
        {\headerfont #1}}{%
        \ifnum \c@secnumdepth >\m@ne
          \if@mainmatter
            {\headerfont\@chapapp\space \thechapter. \space} %
          \fi
        \fi
        {\headerfont #1}%
      }}%
    \def\sectionmark#1{%
      \markright {%
        \ifnum \c@secnumdepth >\z@
          {\headerfont\thesection. \ }%
        \fi
        {\headerfont #1}}}
% \renewcommand{\chaptermark}[1]{%
%   \markboth{% left_head
%     \ifthenelse{\value{secnumdepth}>\m@ne}{%
%       \ifthenelse{\boolean{@mainmatter}}{%
%         {\headerfont\@chapapp\space \thechapter. \space}
%       }{}% fi mainmatter
%     }{}% fi secnumdepth
%     {\headerfont #1}
%   }{% right_head
%     \ifthenelse{\value{secnumdepth}>\m@ne}{%
%       \ifthenelse{\boolean{@mainmatter}}{%
%         \headerfont\@chapapp\space \thechapter. \space
%       }{}% fi mainmatter
%     }{}% fi secnumdepth
%     {\headerfont #1}
%   }
% }
% \renewcommand{\sectionmark}[1]{%
%   \markright{%
%     \ifthenelse{\value{secnumdepth}>\z@}{%
%       {\headerfont\thesection. \ }%
%     }{}
%     {\headerfont #1}
%   }
% }
% are these unnecessary now?
% \newcommand{\subsectionmark}[1]{}
% \newcommand{\subsubsectionmark}[1]{}
% \newcommand{\paragraphmark}[1]{}
% \newcommand{\subparagraphmark}[1]{}
%%
%% Titlepages
%%
\newenvironment{titlepages}{% begin code
  \pagestyle{empty}
  \ \clearpage
  \addtocounter{page}{-1}
}{% end code
  \clearpage
  \pagestyle{fancy}
}
\newboolean{hasauthor}
\newenvironment{authorgroup}{%
\raggedright
\setlength{\parindent}{\z@}
\sffamily\normalsize
%   \begin{minipage}{.7\textwidth}
%     \centering\rmfamily\large
  }{%
%  \end{minipage}
\par
}
\renewcommand{\author}[2]{%
  \ifthenelse{\boolean{hasauthor}}{% not first in list
    \unskip\\%
    \mbox{#1\space{}#2}%
  }{%
    \mbox{#1\space{}#2}%
    \setboolean{hasauthor}{true}%
  }%
}
\newenvironment{copyrights}{\addtolength{\leftskip}{3em}}{}
\newcommand{\published}[2]{\normalsize\rmfamily#1,\\#2}
\newenvironment{annotate}{}{\par}
%%
%% Sectioning
%%
% Let;s first use our predecessor's work
% First of all we need long section titles in ToC
%% FIXME: hack here, we only need \@sect from misccorr
%%\RequirePackage[cp1251]{inputenc}
\RequirePackage[utf8]{inputenc}
\RequirePackage[russian]{babel}
\RequirePackage{misccorr}
% our favourite hack of secdef
\renewcommand{\secdef}[2]{\@ifstar{\@dblarg{#2}}{\@dblarg{#1}}}
% section numbering
% FIXME: move to a proper place -- too flexible?
\setcounter{secnumdepth}{0}
% here we need only to switch pagestyle to empty
\renewcommand{\part}{%
  \if@openright
    \cleardoublepage
  \else
    \clearpage
  \fi
  \thispagestyle{empty}%
  \if@twocolumn
    \onecolumn
    \@tempswatrue
  \else
    \@tempswafalse
  \fi
  \null\vfil
  \secdef\@part\@spart%
}
\newcommand{\partmark}[1]{\markboth{\headerfont #1}{}}
\def\@part[#1]#2{%
    \ifnum \c@secnumdepth >-2\relax
      \refstepcounter{part}%
      \addcontentsline{toc}{part}{\thepart\hspace{1em}#1}%
    \else
      \addcontentsline{toc}{part}{#1}%
    \fi
    \partmark{#1}%\typeout{Part mark #1}%
    {\centering
     \interlinepenalty \@M
     \normalfont
     \ifnum \c@secnumdepth >-2\relax
       \huge\ttfamily \partname~\thepart
       \par
       \vskip 20\p@
     \fi
     \Huge \ttfamily #2\par}%
    \@endpart}
\def\@spart#1{%
    {\centering
     \interlinepenalty \@M
     \normalfont
     \Huge \sffamily\bfseries #1\par}%
    \@endpart}
% was too much empty pages, shrinking
\def\@endpart{\vfil\newpage
%               \if@twoside
%                 \null
%                 \thispagestyle{empty}%
%                 \newpage
%               \fi
              \if@tempswa
                \twocolumn
              \fi}
%% we use standard book.cls counters definitions, not copying them here
% in \chapter definition we only changed pagestyle and rewrite it in LaTeX
\renewcommand{\chapter}{%
  \ifthenelse{\boolean{@openright}}{%
    \cleardoublepage
  }{%
    \clearpage
  }
  \thispagestyle{empty}%
  \global\@topnum\z@
  \setboolean{@afterindent}{false}
  \secdef\@chapter\@schapter
}
% if we have pictures, we need to put them in additional argument to
% \chapter, so redefine
\renewcommand{\@chapter}[3][]{%
  \ifthenelse{\value{secnumdepth}>\m@ne}{%
    \ifthenelse{\boolean{@mainmatter}}{%
      \refstepcounter{chapter}%
      \typeout{\chaptername\space\thechapter.}%
      \addcontentsline{toc}{chapter}%
      {{\thechapter}{#2}}%
    }{% not in main matter
      \addcontentsline{toc}{chapter}%
      {{\thechapter}{#2}}%
    }%
  }{%
    \addcontentsline{toc}{chapter}%
    {{\thechapter}{#2}}%
  }%
  \chaptermark{#1}%
  \ifthenelse{\boolean{@twocolumn}}{%
    \@topnewpage[\@makechapterhead{#2}{#3}]%
  }{%
    \@makechapterhead{#2}{#3}%
    \@afterheading
  }
}
% Chapter heads
\newcommand{\chapternamefont}{\Huge\sffamily}
\newlength\chapternamewidth
\settowidth\chapternamewidth{\chapternamefont\chaptername\space 3\quad}
% design as of master 2.4? 
\renewcommand{\@makechapterhead}[2]{%
  %\vspace*{2\baselineskip}%
  {\setlength{\parindent}{\z@}
    \ifthenelse{\value{secnumdepth}>\m@ne}{%
      \begingroup%
      \chapternamefont\parbox{\chapternamewidth}%
      {\@chapapp\space\thechapter\quad}
      \ifthenelse{\boolean{withpics}}{% with pictures
        \parbox[t]{.6\textwidth}{\includegraphics{#2}}
      }{}% 
      \vspace{\baselineskip}
      \hrule height 1pt
      \vspace{\baselineskip}
      \hfill\parbox[t]{.9\textwidth}{%
        \noindent%
        \raggedleft
        \Huge\sffamily #1\par%
      }%
      \par%
      \vspace{\baselineskip}
      \endgroup%
    }{% unnumbered chapter
      \parbox[t]{.95\textwidth}{%
        \setlength{\parindent}{\z@}
        \addtolength\rightskip{0pt plus 1fil}
        \Huge\sffamily #1}%
      \par%
      \vspace{3\baselineskip}
    }
  }% end of group for all of the chapter head
}
\renewcommand{\@schapter}[2][]{%
  \ifthenelse{\equal{#2}{\contentsname}}{%
  }{% do not add ToC chapter into contents
    \addcontentsline{toc}{chapter}{{}{#2}}
  }
  \@makeschapterhead{#2}%
  \chaptermark{#1}
  \@afterheading
}
\renewcommand{\@makeschapterhead}[1]{%
  \vspace*{1\baselineskip}%
  {\setlength{\parindent}{\z@}
    % \hspace*{\chapternamewidth}
    \parbox{.8\textwidth}{%
      \setlength{\parindent}{\z@}
      \raggedright
      \Huge\sffamily #1}%
    \par
    \ifthenelse{\boolean{usemtc}}{%
      \adjustmtc
    }{}
  }
  \vspace{7\baselineskip}
}
\renewcommand{\thesection}{}
\renewcommand{\thesubsection}{\arabic{subsection}}
\renewcommand{\section}{\@startsection {section}{1}{\z@}%
  {-3.5ex \@plus -1ex \@minus -.2ex}%
  {2.3ex \@plus.2ex}%
  {\normalfont\Large\sffamily\bfseries\raggedright}%
}
\renewcommand{\subsection}{\@startsection{subsection}{2}{\z@}%
  {-3.25ex\@plus -1ex \@minus -.2ex}%
  {1.5ex \@plus .2ex}%
  {\normalfont\large\sffamily\raggedright}%
}
\renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{\z@}%
  {-3.25ex\@plus -1ex \@minus -.2ex}%
  {1.5ex \@plus .2ex}%
  {\normalfont\normalsize\sffamily\raggedright}
}
\renewcommand{\paragraph}{\@startsection{paragraph}{4}{\z@}%
  {1ex \@plus.5ex \@minus.2ex}%
  {-.5em}%
  {\normalfont\normalsize\sffamily}%
}
\renewcommand{\subparagraph}{\@startsection{subparagraph}{5}{\parindent}%
  {1.5ex \@plus.2ex \@minus .2ex}%
  {-.5em}%
  {\normalfont\normalsize\bfseries}%
}
% \@sect was redefined for us by misccorr, so we don't care
%%
%% Table of Contents
%%
% parameter setting
\renewcommand\@pnumwidth{2.2em}
\renewcommand\@tocrmarg{2.5em}
\renewcommand\@dotsep{1}
\setcounter{tocdepth}{3}
%% useful macros -- starred ToC that doesn't change when compiling (for
%% final make-up)
%% FIXME: check, if this was already implemented somewhere or/and move
%% into separate package
%% unfortunately, here it uses local usemtc boolean
\renewcommand\tableofcontents{%
  \setboolean{inmaintoc}{true}%
  \@ifstar{% starred version -- no \@starttoc
    \chapter*{\contentsname}%%
    \makeatletter%
    \input{\jobname.toc}
    \makeatother
  }{% usual version
    \ifthenelse{\boolean{usemtc}}{%
      \dominitoc%
    }{}
    \chapter*{\contentsname}%
    %% ������
    %% \vspace*{-5\baselineskip}
    % \adjustmtc% already done in schapter
    \@starttoc{toc}
  }%
}
\renewcommand*\l@part[2]{%
    \ifnum \c@tocdepth >-2\relax
    \addpenalty{-\@highpenalty}%
    \addvspace{2.25em \@plus\p@}%
    \begingroup
      \parindent \z@ \rightskip \@pnumwidth
      \parfillskip -\@pnumwidth
      {\leavevmode
       \hfil\Large \sffamily\bfseries \partname\space#1\hfil }\par
       \nobreak
         \global\@nobreaktrue
         \everypar{\global\@nobreakfalse\everypar{}}%
    \endgroup
  \fi
}
\renewcommand*\l@chapter[2]{%
  \ifnum \c@tocdepth >\m@ne
    %\addpenalty{-\@highpenalty}%
    \vspace{.7\baselineskip plus 2pt}
    \setlength\@tempdima{1.5em}%
    \begingroup
      \parindent \z@ \rightskip \z@ plus 1 fil%\@pnumwidth
      \leavevmode \sffamily\Large
      \advance\leftskip\@tempdima
      \hskip -\leftskip
      \l@@chapter#1{#2}%
      %\nobreak\hb@xt@\@pnumwidth{\hss #2}%
      \par
      \vspace{1ex plus 2pt}
      \penalty\@highpenalty
    \endgroup
  \fi
}
\newcommand*{\l@@chapter}[3]{%
  \ifthenelse{\equal{#1}{}}{%
    #2\nobreak\hfill\hb@xt@\@pnumwidth{\large\hss #3}
  }{%
    \setlength\parfillskip{0pt plus 1fil}%
    {\large\chaptername\space #1}\\[.5ex] 
    \mdseries #2\nobreak\hfill{\large #3}\relax}
}
% toc sectioning
\renewcommand*\l@section{\addvspace{.1\baselineskip}%
  \@dottedtocline{1}{1.5em}{2.7em}}
\renewcommand*\l@subsection{\addvspace{.1\baselineskip}%
  \@dottedtocline{2}{3em}{3.2em}}
\renewcommand*\l@subsubsection{\addvspace{.1\baselineskip}%
  \@dottedtocline{3}{4.5em}{4.1em}}
\renewcommand*\l@paragraph{\@dottedtocline{4}{10em}{5em}}
\renewcommand*\l@subparagraph{\@dottedtocline{5}{12em}{6em}}
\def\@dottedtocline#1#2#3#4#5{%
  \ifnum #1>\c@tocdepth \else
    \vskip \z@ \@plus.2\p@
    {\leftskip #2\relax \rightskip \@tocrmarg \parfillskip -\rightskip
     \parindent #2\relax\@afterindenttrue
     \interlinepenalty\@M
     \leavevmode
     \@tempdima #3\relax
     \advance\leftskip \@tempdima \null\nobreak\hskip -\leftskip
     \addtolength\rightskip{0pt plus 1fil}
     {\sffamily\upshape\ifx1#1\ifinmaintoc\large\fi\fi #4}\nobreak
     \leaders\hbox{$\m@th
        \mkern \@dotsep mu\hbox{.}\mkern \@dotsep
        mu$}\hfill
     \nobreak
%     \hb@xt@\@pnumwidth{\hfil\sffamily\upshape\ifx1#1\ifinmaintoc\large\fi\fi #5}%
     \hb@xt@\@pnumwidth{\hfil\sffamily\upshape\large #5}%
     \par%
%     \ifx1#1\vspace{.5ex plus .1ex minus .3ex}\@afterheading\fi%
     \vspace{.5ex}\@afterheading%
   }%
   \fi
 }
%%
%% Minitocs
%%
% parameters set in minitoc option
% no need in this -- done in \tableofcontents
%\AtBeginDocument{\dominitoc}
%%
%% Float setup
%%
\renewcommand{\topfraction}{0.9}
\renewcommand{\textfraction}{0.1}
\renewcommand{\floatpagefraction}{1}
%%
%% Captions
%%
\AtBeginDocument{%
\newcommand{\capnamefont}{\sffamily\bfseries}
\newcommand{\captextfont}{\sffamily}
\long\def\@makecaption#1#2{%
  \vskip\abovecaptionskip
% only line that below was changed -- styling
  \sbox\@tempboxa{{\capnamefont #1.} {\captextfont #2}}%
  \ifdim \wd\@tempboxa >\hsize
    {\capnamefont #1.} {\captextfont #2}\par
  \else
    \global \@minipagefalse
    \hb@xt@\hsize{\hfil\box\@tempboxa\hfil}%
  \fi
  \vskip\belowcaptionskip
}
}
%% verbatim tuneup
% \RequirePackage{verbatim}
% \renewcommand{\verbatim@font}{\ttfamily\small}
% \RequirePackage{fancyvrb}
% \RecustomVerbatimEnvironment{Verbatim}{Verbatim}%
% {}
%%%
%%% Environments
%%%
% %%% Notice
% \newlength{\noticeskip}
% \setlength\noticeskip{1ex plus .2ex minus .4ex}
% \newenvironment{notice}[1]{
%   \interlinepenalty=10000
% %  \setlength\leftskip{-\leftmargin}
%   \setlength\rightskip{0pt}
%   \vspace{\noticeskip}
%   \noindent\parbox[c]{18mm}{\includegraphics[totalheight=18mm]{images/img_02.eps}}%
%   \setlength\@tempdima{\linewidth-23mm}%
%   \hfill\begin{minipage}{\@tempdima}\noindent{\sffamily\mdseries#1}\strut\quad\par%
%     \hrule\vspace{1ex}
% }{
%   \end{minipage}\par
%   \vspace{\noticeskip}
% }
%%% Variablelist
\newlength{\termmaxwidth}
\setlength\termmaxwidth{.8\textwidth}
%% \newlength{\variabletermskip}
%% \setlength\variabletermskip{1.5ex plus .2ex minus .1ex}
\newcommand{\termbox}{%
  \
}
\newcommand{\variableterm}[1]{%
  \setlength{\@tempdima}{\linewidth}%
  \advance\@tempdima\leftmargin%
  \advance\@tempdima-\labelwidth%
  \advance\@tempdima-\labelsep%
  \makebox[\@tempdima][l]{%
    \parbox[t]{.8\@tempdima}{\raggedright\ttfamily#1\\%
      \vspace{1.5ex plus .1ex minus 1.ex}
    }%
  }%
}
\newenvironment{variablelist}{% begin-code
  \begin{list}{}{%
%      \renewcommand{\makelabel}[1]{\variableterm{##1}\hfil}
      \let\makelabel\variableterm%
      \setlength\topsep{\noticeskip}
      \setlength\labelwidth{.5em}
      \setlength\leftmargin{1.5em}
      \setlength\rightmargin{0pt}
      \setlength\labelsep{.5em}
      \setlength\partopsep{0pt}
      %\setlength\listparindent{1em}
      %\addtolength\leftmarginii{1em}
   }
}{% end-code
  \end{list}
}
\renewenvironment{description}{%
  \begin{variablelist}
}{%
  \end{variablelist}
}
%%% Additional sectioning interface
%%%
\endinput
%
%% end of file altbook.cls
