#!/bin/sh

# Convert to utf8 from old repository

# Copyright (c) 2008-2009 Etersoft
# Permission is granted to copy, distribute and/or modify this document
# under the terms of the GNU Free Documentation License, Version 1.3
# or any later version published by the Free Software Foundation;

PRIVDOC=../../wine-etersoft-docs-private
FILENAME=$(basename $1)
FILEFROM=$PRIVDOC/$1
FILETO=$2

if [ -r "$FILEFROM" ] ; then
	echo "Converting $FILEFROM..."
	echo "# DO NOT CHANGE! Autoconverted from $FILEFROM by $0 script " >$FILETO
	echo "#" >>$FILETO
	iconv -f koi8-r -t utf8 <$FILEFROM >>$FILETO
else
	echo "Failed to open $FILEFROM"
fi

