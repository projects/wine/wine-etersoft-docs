#!/bin/sh

# Wrapper for use Utfed m-k files
# Вставляет содержание только после <h1>Содержание

# Copyright (c) 2005-2009 Etersoft
# Permission is granted to copy, distribute and/or modify this document
# under the terms of the GNU Free Documentation License, Version 1.3
# or any later version published by the Free Software Foundation;

# if wee need TOC
TOCED=
if [ "$1" = "--toced" ] ; then
	TOCED=1
	shift
fi

fatal()
{
	echo $@
	exit -1
}

# Since html-xml-utils-5.5 toc renamed to hxtoc
TOCCOMMAND=$(which hxtoc 2>/dev/null)
[ -x "$TOCCOMMAND" ] || TOCCOMMAND=$(which toc 2>/dev/null)
[ -x "$TOCCOMMAND" ] || fatal "Missed html-xml-utils package"


INPUT=$1
OUTPUT=$2

# add TOC if needed after H1
toced_filter()
{
	if [ -n "$TOCED" ] ; then
		#cat | sed -e '/<h1>/a\\n<!--toc-->\n' | toc -l 2 | normalize-html
		cat | sed -e '/<h1>.*Содержание/a\\n<!--toc-->\n' | $TOCCOMMAND -l 2
	# FIXME: normalize dropped </body>
	# | normalize-html -l 150
	else
		cat
	fi
}

fatal()
{
	echo "$@" >&2
	exit 1
}

exit_handler()
{
    local rc=$?
    trap - EXIT
    if [ -z "$DEBUG" ] ; then
        rm -f "$TEMPFILE" "$TEMPFILE.html"
    fi
    exit $rc
}

trap exit_handler EXIT HUP INT QUIT PIPE TERM


[ -r "$INPUT" ] || fatal "Can't read input file '$INPUT'"
[ -n "$OUTPUT" ] || fatal "OUTPUT file is not set"

# Используем koi8-r из-за того, что ALDConvert вставляет строки в koi8-r
BYTEENC=KOI8-R
# create temp in the current dir
TEMPFILE=$(mktemp --tmpdir=. tmpXXXXXXXXXX)
#TEMPFILE=$(mktemp)

iconv -f utf8 -t $BYTEENC < "$INPUT" > "$TEMPFILE" || fatal "Can't convert with iconv to $BYTEENC"

echo "ALDConvert -w html \"$TEMPFILE\" \"$TEMPFILE.html\""
LANG=ru_RU.$BYTEENC ALDConvert -r m-k -w html "$TEMPFILE" "$TEMPFILE.html" 2>&1 | iconv -f $BYTEENC -t utf8

[ -n "$(cat "$TEMPFILE.html" 2>/dev/null)" ] || fatal "Can't create output. Check possible errors in '$TEMPFILE'"

cat "$TEMPFILE.html" | sed -e "s|charset=$BYTEENC|charset=UTF-8|g" | \
	iconv -f $BYTEENC -t utf8 | \
	toced_filter \
	> "$OUTPUT" || fatal "Can't convert to output encoding"

