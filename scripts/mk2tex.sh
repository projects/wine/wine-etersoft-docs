#!/bin/sh

# Wrapper for use Utfed m-k files

# Copyright (c) 2005-2009 Etersoft
# Permission is granted to copy, distribute and/or modify this document
# under the terms of the GNU Free Documentation License, Version 1.3
# or any later version published by the Free Software Foundation;

INPUT=$1
OUTPUT=$2

# Checkme: do not correct replace Example in utf8 locale
tex_filter()
{
	sed -e 's,^\\\(begin\|end\){Example}.*$,\\\1{verbatim},' | \
	sed -e 's,\.svg,\.eps,' | \
	sed -e 's,^\\section{\([^}]\+\)},\\chapter{\1}{},' | \
	sed -e 's,subsection{,section{,' | \
	sed -e 's,^\\Figure,&[width=100mm],' | \
	cat
}

fatal()
{
	echo "$@" >&2
	exit 1
}

exit_handler()
{
    local rc=$?
    trap - EXIT
    if [ -z "$DEBUG" ] ; then
        rm -f "$TEMPFILE" "$TEMPFILE.tex"
    fi
    exit $rc
}

trap exit_handler EXIT HUP INT QUIT PIPE TERM

[ -r "$INPUT" ] || fatal "Can't read input file '$INPUT'"
[ -n "$OUTPUT" ] || fatal "OUTPUT file is not set"

# Используем koi8-r из-за того, что ALDConvert вставляет строки в koi8-r 
BYTEENC=KOI8-R

TEXENC=UTF-8

# create temp in the current dir
TEMPFILE=$(mktemp --tmpdir=. tmpXXXXXXXXXX)

iconv -f utf8 -t $BYTEENC < "$INPUT" | \
	sed -e "s|>Содержание||" \
	> "$TEMPFILE" || fatal "Can't convert with iconv to $BYTEENC"

echo "ALDConvert -w latex \"$TEMPFILE\" \"$TEMPFILE.tex\""
LANG=ru_RU.$BYTEENC ALDConvert -r m-k -w latex "$TEMPFILE" "$TEMPFILE.tex" -F 2>&1 #| iconv -f $BYTEENC -t utf8
[ -n "$(cat "$TEMPFILE.tex" 2>/dev/null)" ] || fatal "Can't create output. Check possible errors in '$TEMPFILE'"

# get latex file in $TEXENC
export LANG=ru_RU.$TEXENC
cat "$TEMPFILE.tex" | tex_filter | \
	iconv -f $BYTEENC -t $TEXENC > "$OUTPUT"

