#!/bin/sh -x

# Copyright (c) 2008-2009, 2012 Etersoft
# Permission is granted to copy, distribute and/or modify this document
# under the terms of the GNU Free Documentation License, Version 1.3
# or any later version published by the Free Software Foundation;

fatal()
{
	echo "Failed: $@"
	exit 1
}

[ -n "$1" ] && FILE=$1 || fatal
[ -n "$2" ] && CID=$2 || fatal

. $(dirname $0)/joomla_publish.cfg

# Если задан порт, то формируем соответствующий параметр...
scp_cfgPORT=
ssh_cfgPORT=
[ -n "$PORT" ] && scp_cfgPORT="-P $PORT" && ssh_cfgPORT="-p $PORT";

BFILE=$(basename $FILE)

#  Выполнение команды
scp $scp_cfgPORT $FILE "$SITE:~/tmp/$BFILE" || fatal "can't copy"
scp $scp_cfgPORT $(dirname $0)/publisher.php "$SITE:~/tmp/" || fatal "can't copy"
ssh $ssh_cfgPORT $SITE php -d safe_mode=off $PHPPARAM -f "~/tmp/publisher.php" -- $APOPATH "~/tmp/$BFILE" $CID
echo

