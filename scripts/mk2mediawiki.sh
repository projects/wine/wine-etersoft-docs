#!/bin/sh

# Wrapper for use Utfed m-k files

# Copyright (c) 2005-2009 Etersoft
# Permission is granted to copy, distribute and/or modify this document
# under the terms of the GNU Free Documentation License, Version 1.3
# or any later version published by the Free Software Foundation;

INPUT=$1
OUTPUT=$2

fatal()
{
	echo "$@" >&2
	exit 1
}

exit_handler()
{
    local rc=$?
    trap - EXIT
    if [ -z "$DEBUG" ] ; then
        rm -f "$TEMPFILE" "$TEMPFILE.mediawiki"
    fi
    exit $rc
}

trap exit_handler EXIT HUP INT QUIT PIPE TERM


[ -r "$INPUT" ] || fatal "Can't read input file '$INPUT'"
[ -n "$OUTPUT" ] || fatal "OUTPUT file is not set"

# Используем koi8-r из-за того, что ALDConvert вставляет строки в koi8-r
BYTEENC=KOI8-R
# create temp in the current dir
TEMPFILE=$(mktemp --tmpdir=. tmpXXXXXXXXXX.m-k)
#TEMPFILE=$(mktemp)

iconv -f utf8 -t $BYTEENC < "$INPUT" > "$TEMPFILE" || fatal "Can't convert with iconv to $BYTEENC"

echo "ALDConvert -w mediawiki \"$TEMPFILE\" \"$TEMPFILE.mediawiki\""
LANG=ru_RU.$BYTEENC ALDConvert -r m-k -w mediawiki "$TEMPFILE" "$TEMPFILE.mediawiki" 2>&1 | iconv -f $BYTEENC -t utf8

[ -n "$(cat "$TEMPFILE.mediawiki" 2>/dev/null)" ] || fatal "Can't create output. Check possible errors in '$TEMPFILE'"

cat "$TEMPFILE.mediawiki" | \
	iconv -f $BYTEENC -t utf8 | \
	cat > "$OUTPUT"

[ -s "$OUTPUT" ] && exit

rm -f "$OUTPUT"
exit 1
