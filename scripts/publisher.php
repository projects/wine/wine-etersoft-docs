<?php
        global $argc, $argv;

        if( $argc < 4 )
        {
                echo "\nUsage: ".$argv[0]." rootdir file.html contentID\n";
                exit();
        }

        $rootdir = $argv[1];
        $file = $argv[2];
        $cid  = $argv[3];

        echo " rootdir: ".$rootdir." file: ".$file." cid: ".$cid."\n";

// For Joomla before 1.6
//$table_prefix = 'jos';

// For Joomla 1.7
$table_prefix = 'j17';

require_once( $rootdir.'/configuration.php' );

$jc = new JConfig();

$link = mysql_connect($jc->host, $jc->user, $jc->password);
if (!$link) {
    die('Could not connect: ' . mysql_error());
}

mysql_select_db($jc->db, $link);

mysql_query("SET NAMES 'utf8'", $link );


        $fd = fopen($file,"r");
        if( !$fd )
        {
                echo "<script> alert('"._PUB_FILE_NOT_OPEN.": $fname'); window.history.go(-1); </script>\n";
                exit(1);
        }

        $text = fread( $fd, filesize($file) );
        fclose( $fd );

        @unlink($file);

        if( !preg_match_all("/<[\s]*body[^>]*>([\w\W]*)<\/[\s]*body[^>]*>/i", $text, $body) )
        {
                echo "<script> alert('"._PUB_BODY_NOT_FOUND."'); window.history.go(-1); </script>\n";
                exit(1);
        }
        $newbody = mysql_real_escape_string($body[1][0]);

        $res = mysql_query("UPDATE ".$table_prefix."_content SET introtext='$newbody' WHERE id=$cid", $link);
        if ($res == FALSE)
            die('query error: ' . mysql_error());

        //$content->set('fulltext','');

echo 'Connected successfully\n';
mysql_close($link);

?>
