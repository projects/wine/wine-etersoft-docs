#!/bin/sh

# Convert to utf8 is already not

# Copyright (c) 2008-2009 Etersoft
# Permission is granted to copy, distribute and/or modify this document
# under the terms of the GNU Free Documentation License, Version 1.3
# or any later version published by the Free Software Foundation;

for i in $@ ; do
	echo "Converting $i..."
	if enconv -g $i | grep -qs "UTF-8" ; then
		echo "SKIPPED"
		continue
	fi
	TMPFILE=$(mktemp --tmpdir=.)
	iconv -f koi8-r -t utf8 <$i >$TMPFILE || continue
	cat $TMPFILE >$i
	rm -f $TMPFILE
done
